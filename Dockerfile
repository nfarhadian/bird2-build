FROM debian:bookworm-slim
ARG version
RUN set -e -x \
    && export DEBIAN_FRONTEND=noninteractive \
    && apt-get update \
    && apt-get -y upgrade \
    && apt-get -y --no-install-recommends install libreadline8 \
    && dpkg-query -f '${binary:Package}\n' -W | sort > /tmp/base_packages \
    && apt-get -y --no-install-recommends install \
        curl gcc libc6-dev make bison flex libncurses-dev libreadline-dev \
    && curl -s -S "ftp://bird.network.cz/pub/bird/bird-${version}.tar.gz" | tar xz \
    && cd bird* \
    && ./configure --sysconfdir='/etc/bird'\
    && make \
    && strip -s bird birdc birdcl \
    && make install \
    && cd .. \
    && rm -rf bird* \
    && dpkg-query -f '${binary:Package}\n' -W | sort > /tmp/packages \
    && comm -13 /tmp/base_packages /tmp/packages | xargs apt-get -y purge \
    && rm -f /tmp/base_packages /tmp/packages \
    && rm -rf /var/lib/apt/lists/* \
    && mkdir -p /var/run/bird
ENTRYPOINT []
CMD ["/usr/local/sbin/bird", "-f"]
